FROM debian:buster
MAINTAINER dj3ei@famsik.de

# This is mainly for my convenience, as it uses the Debian server
# that happens to serve my place fastes. Delete this line if you don't like it.
COPY ["sources.list", "/etc/apt/sources.list"]

COPY ["generate_wordlist.py", "count_letters.py", "letter2bitmask.py", "mk_bin_file.py", "find_learning_order.py", "", "/usr/local/bin/"]
COPY ["letters_rare_first.py", "mk_lessons.py", "/usr/local/lib/python3.7/dist-packages/"]
COPY ["tasks.py", "/"]
RUN set -x; apt-get update && apt-get --yes upgrade && \
  apt-get --yes install hunspell-de-de-frami hunspell-en-us hunspell-en-gb hunspell-tools python3-pip && \
  pip3 install invoke && \
  ln /usr/local/bin/generate_wordlist.py /usr/local/bin/count_letters.py /usr/local/bin/letter2bitmask.py /usr/local/bin/mk_bin_file.py /usr/local/bin/find_learning_order.py /usr/local/lib/python3.7/dist-packages && \
  python3 -m compileall && \
  mkdir -p /fromhost

