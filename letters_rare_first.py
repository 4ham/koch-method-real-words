import re

class UnparseableLineError(Exception):
    def __init__(self, line):
        Exception.__init__(self, "Problem occured in line: \"{}\"".format(line))

def from_lettercount_file(wlf):
    """Convert a wordlist file into a simple list of letters, sorted by reverse order in that file."""
    result = []
    for line in wlf:
        mo = re.fullmatch(r'(.):\s\d+\n?', line)
        if mo:
            result.append(mo.group(1))
        else:
            raise UnparseableLineError(line)
    result.reverse()
    return result

