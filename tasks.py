from glob import glob
from invoke import task, Collection
import json
import os
from os.path import isdir, isfile

import count_letters
import find_learning_order
import generate_wordlist
import mk_bin_file
import mk_lessons

languages = ["de_DE", "en_US", "en_GB"]

ns = Collection()

def language2short(language):
    """It is convenient to reduces these to shorter."""
    return language.replace("_", "-").lower() # would also work
    # return language[3:].lower()

def mk_tasks_for_language(language, lang_short):
    """Construct the tasks for one language"""
    tasks = Collection()
    pre_all = []
    dir = language
    
    if not isdir(dir):
        @task(name=f"mkdir-{lang_short}")
        def mkdir(c):
            print(f"Create subdir {dir} of current directory.")
            os.mkdir(dir)
        mkdir.__doc__ = f"Create subdir {dir} of current directory."

        pre_all.append(mkdir)
        tasks.add_task(mkdir)
        pre_wordlist = [mkdir]
    else:
        pre_wordlist = []

    wordlist_file = f"{dir}/wordlist.txt"
    if not isfile(wordlist_file):

        @task(name=f"mk-wordlist-{lang_short}", pre=pre_wordlist)
        def mk_wordlist(c):
            print(f"Create {wordlist_file}.")
            with open(wordlist_file, "w") as outfile:
                generate_wordlist.generate(outfile, language, 6)
        mk_wordlist.__doc__ = f"Create {wordlist_file}."

        pre_all.append(mk_wordlist)
        tasks.add_task(mk_wordlist)
        pre_lettercount = [mk_wordlist]
    else:
        pre_lettercount = []

    lettercount_file = f"{dir}/lettercount.txt"
    if not isfile(lettercount_file):

        @task(name=f"mk-lettercount-{lang_short}", pre=pre_lettercount)
        def mk_lettercount(c):
            print(f"Create {lettercount_file}.")
            with open(wordlist_file, "r") as infile:
              with open(lettercount_file, "w") as outfile:
                  count_letters.count(infile, outfile)
        mk_lettercount.__doc__ = f"Create {lettercount_file}."

        pre_all.append(mk_lettercount)
        tasks.add_task(mk_lettercount)
        pre_db = [mk_lettercount]
    else:
        pre_db = []

    database_file = f"{dir}/letterset2count.kmrw"
    if not isfile(database_file):

        @task(name=f"mk-database-{lang_short}", pre=pre_db)
        def mk_db(c):
            print(f"Creating {database_file}.")
            predb_file = f"{dir}/letterset2count.kmrw.in-the-making"
            with open(lettercount_file, "r") as letterc:
                with open(wordlist_file, "r") as wordl:
                    with open(predb_file, mode="w+b") as binfile:
                        mk_bin_file.mk(letterc, wordl, binfile)
            os.rename(predb_file, database_file)
        mk_db.__doc__ = f"Create {database_file}."

        pre_all.append(mk_db)
        tasks.add_task(mk_db)
        pre_learnorder = [mk_db]
    else:
        pre_learnorder = []

    learn_order_file = f"{dir}/learning_order.json"
    if not isfile(learn_order_file):
    
        @task(name=f"mk-learnorder-{lang_short}", pre=pre_learnorder)
        def mk_learnorder(c):
            with open(lettercount_file, "r") as letterc:
                with open(database_file, mode="rb") as binfile:
                    learning_order = find_learning_order.find(letterc, 120, binfile)
            with open(learn_order_file, "w") as lof:
                json.dump(learning_order, lof, indent=2)
            print(f"Proposed learning order written to {learn_order_file}.")
        mk_learnorder.__doc__ = f"Propose learning order to {learn_order_file}."

        pre_all.append(mk_learnorder)
        tasks.add_task(mk_learnorder)
        pre_lessons = [mk_learnorder]
    else:
        pre_lessons = []

    existing_lesson_files = glob(f"{dir}/lesson_[012][0123456789]_[0123456789].txt")
    if len(existing_lesson_files) < (26 - 2) * 10: # Two characters disappear into the first lesson.

        @task(name=f"mk-lessons-{lang_short}", pre=pre_lessons)
        def mk_lsns(c):
            print(f"Generating lessons files in {dir}.")
            with open(wordlist_file, "r") as wlf:
                wordlist = [line.rstrip() for line in wlf]
            with open(learn_order_file, "r") as lof:
                learn_order = json.load(lof)
            mk_lessons.mk_lessons(wordlist, learn_order, dir)
        pre_all.append(mk_lsns)
        tasks.add_task(mk_lsns)        

    @task(name=f"all-{lang_short}", default=True, pre=pre_all)
    def all(c):
        print(f"Processing has been completed for {language}.")
    all.__doc__ = f"All for {language}."
    tasks.add_task(all)
    return tasks

for language in languages:
    lang_short = language2short(language)
    ns.add_collection(mk_tasks_for_language(language, lang_short), name=lang_short)

all_pre_names =  [f"{ls}.all-{ls}" for ls in
              [language2short(language) for language in languages]]

all_pre = [ns[pre_name] for pre_name in all_pre_names]

@task(pre=all_pre)
def all(c):
    """Do all for all languages."""
    print("Processing completed for all languages.")

ns.add_task(all)

